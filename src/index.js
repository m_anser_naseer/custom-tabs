import React, { Component } from 'react';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import Tab from './Tab';
import './palette.css'

class Tabs extends Component {
  render() {
    const { tabs, style, activeStyle, addable, closeable, draggable } = this.props;
    return (
      <div className="react-tabs-container" style={style}>
        {tabs.map((tab, i) => (
          <Tab
            key={tab.id}
            index={i}
            id={tab.id}
            content={tab.content}
            moveTab={draggable ? this.props.moveTab : ''}
            selectTab={this.props.selectTab}
            closeTab = {closeable ? this.props.closeTab : ''}
            disabled = {tab.disabled}
            active={tab.active}
            activeStyle={activeStyle}
          />
        ))}
        <div className="react-tabs-child">
            {React.Children.toArray(this.props.children)}
        </div>
      </div>
    );
  }
}

Tabs.defaultProps = {
  addable : true,
  closeable: true,
  draggable: true,
};

export default DragDropContext(HTML5Backend)(Tabs)
